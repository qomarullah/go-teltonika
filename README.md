# GO TELTONIKA
TCP server teltonika codec8


## How to build binary
```
./build.sh
```

## How to run
Run use configuration file based on environment.
default ENV=local, configuration will use config.local.json

### Command
```
export ENV={local/dev/prod} && ./go-teltonika 
```

## Configuration
```
{
    "apps": {
      "name": "go-teltonika",
      "address": "0.0.0.0",
      "port": 1883
    },
    "logger": {
      "fileTdrLocation": "logs/tdr.log",
      "fileSyslogLocation": "logs/syslog.log",
      "fileMaxAge": 30,
      "stdout": true
    },
    "mysql": {
        "enable": false,
        "host": "localhost",
        "username": "apps",
        "password": "aplikasi",
        "database": "connectinc_iot",
        "port": 3306,
        "maxIdleConn": 10,
        "maxOpenConn": 30,
        "maxLifeTime": 900
    },
    "http": {
      "enable": false,
      "method": "GET",
      "url": "http://localhost:1234/submit",
      "timeout": 10000
    },
    "kafka": {
      "enable": false,
      "clientId": "go-teltonika",
      "brokers": ["localhost:9092"],
      "topics": "teltonika"
    }
  }
  
```
