package main

import (
	"bufio"
	"fmt"
	"net"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/qomarullah/go-teltonika/codec8"
	"go.uber.org/zap"
)

func main() {

	//setup config
	env := os.Getenv("ENV")
	if env == "" {
		env = "local"
	}

	if len(os.Args) > 1 {
		env = os.Args[1]
	}

	conf := viper.New()
	conf.AddConfigPath(".")
	conf.SetConfigName("config." + env)

	err := conf.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	//setup log
	logger := SetupLogger(conf)

	//check db
	if conf.GetBool("mysql.enable") {
		NewDbMySQL(conf, logger)
	}
	//check kafka
	if conf.GetBool("kafka.enable") {
		NewKafkaProducer(conf, logger)
	}

	// Listen for incoming connections.
	l, err := net.Listen("tcp", conf.GetString("apps.address")+":"+conf.GetString("apps.port"))
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	fmt.Println("tcp server running " + env + " port :" + conf.GetString("apps.port"))
	// Close the listener when the application closes.
	defer l.Close()
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn, conf, logger)
	}
}

// Handles incoming requests.
func handleRequest(conn net.Conn, conf *viper.Viper, logger *Logger) {
	defer conn.Close()
	message := codec8.New()
	step := 1
	for {
		// Make a buffer to hold incoming data.
		buf := make([]byte, 2048)
		reqLen, err := conn.Read(buf)
		if err != nil {
			logger.syslog.Debug("handleRequest", zap.String("error", err.Error()))
			logger.syslog.Debug("handleRequest", zap.String("msg", "close connection.."))
			return
		}

		data := fmt.Sprintf("%x", buf[0:reqLen])
		logger.syslog.Debug("handleRequest", zap.String("msg", data))

		w := bufio.NewWriter(conn)
		if step == 1 {
			//auth & register imei
			w.Write([]byte{1})
			w.Flush()
			message.Imei = string(buf[3:reqLen])
			logger.syslog.Debug("handleRequest", zap.Any("imei", message.Imei))
			step++
		} else {
			if len(data) < 30 {
				logger.syslog.Debug("handleRequest", zap.String("msg", "invalid length"))
				break
			}
			//parse data
			err := message.Decode(data)
			if err != nil {
				logger.syslog.Debug("handleRequest", zap.String("error", "parsing"+err.Error()))
				break
			}
			go handleRequestExtend(conf, message, logger)
			logger.syslog.Debug("handleRequest", zap.Any("response-NumberOfData", message.NumberOfData))

			w.Write([]byte{0, 0, 0, uint8(message.NumberOfData)})
			w.Flush()
			logger.tdr.Info("", zap.Any("message", message))
			break
		}

	}
}
func handleRequestExtend(conf *viper.Viper, message *codec8.Message, logger *Logger) {

	if conf.GetBool("mysql.enable") {
		fmt.Println("submit-mysql")
		SubmitMySQLCodec8(conf, message, logger)
	}
	if conf.GetBool("http.enable") {
		fmt.Println("submit-http")
		SubmitHTTPCodec8(conf, message, logger)

	}
	if conf.GetBool("kafka.enable") {
		fmt.Println("submit-kafka")
		SubmitKafkaCodec8(conf, message, logger)
	}

}
