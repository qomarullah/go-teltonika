package main

import (
	"bytes"
	"crypto/tls"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/Shopify/sarama"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/cast"
	"github.com/spf13/viper"
	"gitlab.com/qomarullah/go-teltonika/codec8"
	"gitlab.com/qomarullah/go-teltonika/logger"
	"go.uber.org/zap"
)

type Logger struct {
	tdr    *zap.Logger
	syslog *zap.Logger
}

func SetupLogger(conf *viper.Viper) *Logger {

	loggerTdr := logger.NewZapLogger(conf.GetBool("logger.stdout"), conf.GetString("logger.fileTdrLocation"), conf.GetDuration("logger.fileMaxAge"))
	loggerSyslog := logger.NewZapLogger(conf.GetBool("logger.stdout"), conf.GetString("logger.fileSyslog Location"), conf.GetDuration("logger.fileMaxAge"))

	/*logFileTdr := conf.GetString("logger.fileTdrLocation") + ".%Y%m%d"
	logFileSyslog := conf.GetString("logger.fileSyslogLocation") + ".%Y%m%d"
	rotator, err := rotatelogs.New(
		logFileTdr,
		rotatelogs.WithMaxAge(60*24*time.Hour),
		rotatelogs.WithRotationTime(time.Hour))
	if err != nil {
		panic(err)
	}
	// initialize the JSON encoding config
	encoderConfig := map[string]string{
		//"levelEncoder": "capital",
		"timeKey":     "timestamp",
		"timeEncoder": "RFC339",
	}
	data, _ := json.Marshal(encoderConfig)
	var encCfg zapcore.EncoderConfig
	if err := json.Unmarshal(data, &encCfg); err != nil {
		panic(err)
	}

	// add the encoder config and rotator to create a new zap logger
	w := zapcore.AddSync(rotator)
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(encCfg),
		w,
		zap.InfoLevel)
	logger := zap.New(core)
	*/

	return &Logger{tdr: loggerTdr, syslog: loggerSyslog}

}

var (
	onceDb sync.Once
	db     *sql.DB
)

func NewDbMySQL(conf *viper.Viper, logger *Logger) *sql.DB {
	onceDb.Do(func() {
		dbDriver := "mysql"
		dbUser := conf.GetString("mysql.username")
		dbPass := conf.GetString("mysql.password")
		dbName := conf.GetString("mysql.database")
		dbConn, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
		if err != nil {
			logger.syslog.Error("NewDbMySQL", zap.Any("error", err))
			panic(err)
		}
		db = dbConn
	})
	return db
}

func SubmitMySQLCodec8(conf *viper.Viper, message *codec8.Message, logger *Logger) error {
	if message == nil || len(message.AvlData) == 0 {
		return errors.New("Not found message")
	}

	var err error
	db = NewDbMySQL(conf, logger)
	defer db.Close()

	sql, err := db.Prepare("INSERT INTO teltonika(imei,timestamp,latitude,longitude,priority,altitude,angle,satelites,speed) VALUES(?,?,?,?,?,?,?,?,?)")
	if err != nil {
		logger.syslog.Error("SubmitMySQLCodec8", zap.Any("error", err))
		return err
	}

	timestamp := message.AvlData[0].Timestamp
	priority := message.AvlData[0].Priority
	latitude := message.AvlData[0].Latitude
	longitude := message.AvlData[0].Longitude
	altitude := message.AvlData[0].Altitude
	angle := message.AvlData[0].Angle
	satelites := message.AvlData[0].Satelites
	speed := message.AvlData[0].Speed

	_, err = sql.Exec(message.Imei, timestamp, latitude, longitude, priority, altitude, angle, satelites, speed)
	if err != nil {
		logger.syslog.Error("SubmitMySQLCodec8", zap.Any("error", err))

	}
	return err

}

func HitAPI(url string, method string, timeout int, headers map[string]string, payload string) (body []byte, err error) {

	req, err := http.NewRequest(method, url, bytes.NewBuffer([]byte(payload)))
	if err != nil {
		return
	}

	for k, v := range headers {
		req.Header.Set(k, v)
	}
	to := time.Duration(time.Duration(timeout) * time.Second)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Timeout:   to,
		Transport: tr,
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	return
}

func SubmitHTTPCodec8(conf *viper.Viper, message *codec8.Message, logger *Logger) error {
	if message == nil || len(message.AvlData) == 0 {
		return errors.New("Not found message")
	}

	var err error
	url := conf.GetString("http.url")
	method := conf.GetString("http.method")
	timeout := conf.GetInt("http.timeout")

	headers := map[string]string{}
	headers["Content-Type"] = "application/json"
	var body string

	timestamp := message.AvlData[0].Timestamp
	priority := cast.ToString(message.AvlData[0].Priority)
	latitude := cast.ToString(message.AvlData[0].Latitude)
	longitude := cast.ToString(message.AvlData[0].Longitude)
	altitude := cast.ToString(message.AvlData[0].Altitude)
	angle := cast.ToString(message.AvlData[0].Angle)
	satelites := cast.ToString(message.AvlData[0].Satelites)
	speed := cast.ToString(message.AvlData[0].Speed)

	if method == "GET" {
		url += "imei=" + cast.ToString(message.Imei) + "&timestamp=" + timestamp + "&priority=" + priority + "&latitude=" + latitude + "&longitude=" + longitude + "&altitude=" + altitude + "&angle=" + angle + "&satelites=" + satelites + "&speed=" + speed
	} else {
		bodyString := conf.GetString("http.body")
		bodyString = strings.Replace(bodyString, "[longitude]", longitude, -1)
		bodyString = strings.Replace(bodyString, "[latitude]", latitude, -1)
		body = bodyString
	}
	logger.syslog.Info("SubmitHTTPCodec8", zap.Any("http", url), zap.Any("body", body))
	response, err := HitAPI(url, method, timeout, headers, string(body))

	if err != nil {
		logger.syslog.Error("SubmitHTTPCodec8", zap.Any("error", err))

	}
	logger.syslog.Info("SubmitHTTPCodec8", zap.Any("response", response))
	return err

}

var (
	onceKafka sync.Once
	kafka     sarama.SyncProducer
)

func NewKafkaProducer(conf *viper.Viper, logger *Logger) sarama.SyncProducer {
	onceKafka.Do(func() {
		kafkaConfig := sarama.NewConfig()
		kafkaConfig.ClientID = conf.GetString("kafka.clientId")
		kafkaConfig.Producer.Return.Errors = true
		kafkaConfig.Producer.Return.Successes = true
		kafkaBrokers := conf.GetStringSlice("kafka.brokers")
		kafkaProducer, err := sarama.NewSyncProducer(kafkaBrokers, kafkaConfig)
		if err != nil {
			//fmt.Println("SetupKafka-error", err)
			logger.syslog.Error("NewKafkaProducer", zap.Any("error", err))
			panic(err)
		}
		kafka = kafkaProducer

	})
	return kafka
}

func SubmitKafkaCodec8(conf *viper.Viper, message *codec8.Message, logger *Logger) error {

	if message == nil || len(message.AvlData) == 0 {
		return errors.New("Not found message")
	}

	kafkaProducer := NewKafkaProducer(conf, logger)
	topic := conf.GetString("kafka.topic")

	reqByte, err := json.Marshal(message)
	if err != nil {
		return err
	}
	msg := string(reqByte)
	kafkaMsg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(msg),
	}
	partition, offset, err := kafkaProducer.SendMessage(kafkaMsg)
	if err != nil {
		logger.syslog.Error("SubmitKafkaCodec8", zap.Any("error", err))
		return err
	}
	logger.syslog.Debug("SubmitKafkaCodec8", zap.Any("partition", partition), zap.Any("offset", offset))

	return err
}
